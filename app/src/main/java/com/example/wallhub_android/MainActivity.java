package com.example.wallhub_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {


    TextInputEditText emailEditText, passwordEditText;
    MaterialButton logIn;
    TextView newUser, resetPassword;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        logIn = findViewById(R.id.inicioSesion);
        newUser = findViewById(R.id.nuevoUsuario);
        resetPassword = findViewById(R.id.resetPassword);

        mAuth = FirebaseAuth.getInstance();

        logIn.setOnClickListener(v -> validate());


        newUser.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
            startActivity(intent);
        });

        resetPassword.setOnClickListener(v -> {
            if (emailEditText.getText() != null) {
                mAuth.sendPasswordResetEmail(Objects.requireNonNull(emailEditText.getText()).toString().trim())
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Toast.makeText(MainActivity.this, "Email sent", Toast.LENGTH_LONG).show();
                            }
                        });
            } else {
                Toast.makeText(MainActivity.this, "Put your mail on the field", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void validate() {
        String email = Objects.requireNonNull(emailEditText.getText()).toString().trim();
        String password = Objects.requireNonNull(passwordEditText.getText()).toString().trim();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailEditText.setError("Invalid mail");
            return;
        } else {
            emailEditText.setError(null);
        }

        if (password.isEmpty() || password.length() < 8){
            passwordEditText.setError("Minimum 8 characters");
            return;
        } else if (!Pattern.compile("[0-9]").matcher(password).find()){
            passwordEditText.setError("Minimum 1 number");
            return;
        } else if (!Pattern.compile("[A-Z]").matcher(password).find()){
            passwordEditText.setError("Minimum 1 upper case");
            return;
        } else{
            passwordEditText.setError(null);
        }
        logIn(email, password);
    }

    public void logIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        assert user != null;
                        if (user.isEmailVerified()) {
                            Intent intent = new Intent(MainActivity.this, FeedActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else{
                            Toast.makeText(MainActivity.this, "Validate Mail", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Incorrect credentials", Toast.LENGTH_LONG).show();
                    }
                });
    }
}