package com.example.wallhub_android;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wallhub_android.adapters.PostListAdapter;
import com.example.wallhub_android.classes.Comment;
import com.example.wallhub_android.classes.Post;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class UserAreaActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView nicknameTextView, biographyTextView;
    ImageView avatarImageView, backImageView, followImageView, unfollowImageView;
    ArrayList<Post> postArrayList = new ArrayList<>();
    List<String> friendsArrayList = new ArrayList<>();
    Post post;
    Comment comment;
    String id;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_area);

        recyclerView = findViewById(R.id.postRecyclerView);
        nicknameTextView = findViewById(R.id.nicknameTextView);
        biographyTextView = findViewById(R.id.biographyTextView);
        avatarImageView = findViewById(R.id.avatarImageView);
        backImageView = findViewById(R.id.backImageView);
        followImageView = findViewById(R.id.followImageView);
        unfollowImageView = findViewById(R.id.unfollowImageView);

        db.collection("users").document(user.getUid()).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    friendsArrayList = (List<String>) document.getData().get("friends");
                    if (user.getUid().equals(id)){
                        followImageView.setVisibility(View.GONE);
                    } else if (friendsArrayList.contains(id)) {
                        followImageView.setVisibility(View.GONE);
                        unfollowImageView.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


        followImageView.setOnClickListener(v -> {
            db.collection("users").document(user.getUid()).update("friends", FieldValue.arrayUnion(id));
            followImageView.setVisibility(View.GONE);
            unfollowImageView.setVisibility(View.VISIBLE);
        });

        unfollowImageView.setOnClickListener(v -> {
            db.collection("users").document(user.getUid()).update("friends", FieldValue.arrayRemove(id));
            followImageView.setVisibility(View.VISIBLE);
            unfollowImageView.setVisibility(View.GONE);
        });

        backImageView.setOnClickListener(v -> {
            Intent intent = new Intent(UserAreaActivity.this, FeedActivity.class);
            startActivity(intent);
            finish();
        });

        try {
            post = (Post) getIntent().getSerializableExtra("Post");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            comment = (Comment) getIntent().getSerializableExtra("Comment");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (post != null) {
            id = post.getPostOwner();
        } else if (comment != null) {
            id = comment.getUserOwner();
        } else {
            id = getIntent().getStringExtra("id");
        }

            db.collection("users").document(id)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            try {
                                assert document != null;
                                nicknameTextView.setText(document.getString("nickname"));
                                biographyTextView.setText(document.getString("biography"));
                                Picasso.get().load(document.getString("avatar")).fit().centerCrop().into(avatarImageView);
                                insertPostData(id);
                            } catch (Exception e) {
                                System.out.println(e.toString());
                            }
                        }
                    });
    }


    public void insertPostData(String id){
        db.collection("users").document(id)
                .collection("post")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                            postArrayList.add(new Post(document.getString("message"),
                                    id,
                                    document.contains("image") ? document.getString("image") : null,
                                    (List<String>) document.getData().get("reactions"),
                                    document.getId(),
                                    document.getString("publishDate"),
                                    document.getString("media_content_type")));
                        }
                        Collections.sort(postArrayList, Post.PostPublishDateComparator);
                        PostListAdapter listAdapter = new PostListAdapter(postArrayList, UserAreaActivity.this);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(UserAreaActivity.this));
                        recyclerView.setAdapter(listAdapter);
                    }
                });
    }
}