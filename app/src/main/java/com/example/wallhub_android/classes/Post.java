package com.example.wallhub_android.classes;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

public class Post implements Serializable {
    String message, postOwner, id, media, publishDate, isImage;
    List<String> reactions;

    public Post(String message, String postOwner, String media , List<String> reactions, String id, String publishDate, String isImage) {
        this.message = message;
        this.postOwner = postOwner;
        this.media = media;
        this.reactions = reactions;
        this.id = id;
        this.publishDate = publishDate;
        this.isImage = isImage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPostOwner() {
        return postOwner;
    }

    public void setPostOwner(String postOwner) {
        this.postOwner = postOwner;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public List<String> getReactions() {
        return reactions;
    }

    public int getReactionsNumber() {
        return reactions.size();
    }

    public void setReactions(List<String> reactions) {
        this.reactions = reactions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsImage() {
        return isImage;
    }

    public void setIsImage(String isImage) {
        this.isImage = isImage;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public static Comparator<Post> PostPublishDateComparator = (p1, p2) -> {
        String publishDate1 = p1.getPublishDate().toUpperCase();
        String publishDate2 = p2.getPublishDate().toUpperCase();

        return publishDate2.compareTo(publishDate1);
    };
}
