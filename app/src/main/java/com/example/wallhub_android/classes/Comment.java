package com.example.wallhub_android.classes;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

public class Comment implements Serializable {
    String userOwner, content, releaseDate, id, postId, postOwner;
    List<String> reactions;

    public Comment(String userOwner, String content, String releaseDate, List<String> reactions, String id, String postId, String postOwner) {
        this.userOwner = userOwner;
        this.content = content;
        this.releaseDate = releaseDate;
        this.reactions = reactions;
        this.id = id;
        this.postId = postId;
        this.postOwner = postOwner;
    }

    public String getUserOwner() {
        return userOwner;
    }

    public void setUserOwner(String userOwner) {
        this.userOwner = userOwner;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<String> getReactions() {
        return reactions;
    }

    public void setReactions(List<String> reactions) {
        this.reactions = reactions;
    }

    public int getReactionsNumber() { return reactions.size(); }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostOwner() {
        return postOwner;
    }

    public void setPostOwner(String postOwner) {
        this.postOwner = postOwner;
    }

    public static Comparator<Comment> CommentPublishDateComparator = (c1, c2) -> {
        String releaseDate1 = c1.getReleaseDate().toUpperCase();
        String releaseDate2 = c2.getReleaseDate().toUpperCase();

        return releaseDate2.compareTo(releaseDate1);
    };
}
