package com.example.wallhub_android.feed_activity_ui.user;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wallhub_android.MainActivity;
import com.example.wallhub_android.adapters.PostListAdapter;
import com.example.wallhub_android.classes.Post;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import static com.example.wallhub_android.NewPostActivity.PICK_IMAGE;

public class UserFragment extends Fragment {

    private com.example.wallhub_android.databinding.FragmentUserBinding binding;

    RecyclerView recyclerView;
    ImageView logout, editUser, avatarImageView;
    TextView nicknameTextView, biographyTextView;
    EditText nicknameEditText, biographyEditText, phoneEditText, passwordEditText, repeatPasswordEditText;
    StorageReference storageRef = FirebaseStorage.getInstance().getReference("/");
    Button saveButton;
    InputStream inputStream;
    Uri image;
    boolean isEditing = false;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = com.example.wallhub_android.databinding.FragmentUserBinding.inflate(inflater, container, false);
        recyclerView = binding.postRecyclerView;
        logout = binding.logoutImageView;
        avatarImageView = binding.avatarImageView;
        nicknameTextView = binding.nicknameTextView;
        biographyTextView = binding.biographyTextView;
        nicknameEditText = binding.nicknameEditText;
        biographyEditText = binding.biographyEditText;
        phoneEditText = binding.phoneEditText;
        passwordEditText = binding.passwordEditText;
        repeatPasswordEditText = binding.repeatPasswordEditText;
        saveButton = binding.saveButton;
        editUser = binding.editUserImageView;

        callPage();

        logout.setOnClickListener(v -> {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
            requireActivity().finish();
        });

        saveButton.setOnClickListener(v -> validateNewUserData());

        editUser.setOnClickListener(v -> {
            isEditing = !isEditing;
            callPage();
        });

        if (user != null) {
            insertUserData();
        }

        avatarImageView.setOnClickListener(v -> {
            if (isEditing){
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                startActivityForResult(chooserIntent, PICK_IMAGE);
            }
        });

        return binding.getRoot();
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContext().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void validateNewUserData() {
        String nickname = nicknameEditText.getText().toString().trim();
        String biography = biographyEditText.getText().toString().trim();
        String phone = phoneEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        String repeatPassword = repeatPasswordEditText.getText().toString().trim();

        if (password.equals("")){
            changeUser(nickname, biography, phone, password);
        } else if (password.length() < 8) {
            passwordEditText.setError("Minimum 8 characters");
            return;
        } else if (!Pattern.compile("[0-9]").matcher(password).find()) {
            passwordEditText.setError("Minimum 1 number");
            return;
        } else if (!Pattern.compile("[A-Z]").matcher(password).find()) {
            passwordEditText.setError("Minimum 1 upper case");
            return;
        } else {
            passwordEditText.setError(null);
        }

        if (!repeatPassword.equals(password)) {
            repeatPasswordEditText.setError("Must be equals");
        } else {
            if (!password.equals(""))
                changeUser(nickname, biography, phone, password);
        }
    }

    private void changeUser(String nickname, String biography, String phone, String password) {
        if (!nickname.equals("") && nickname.length() < 16) {
            db.collection("users").document(user.getUid())
                    .update("nickname", nickname);
        }
        if (!biography.equals("") && biography.length() > 1 && biography.length() < 300) {
            db.collection("users").document(user.getUid())
                    .update("biography", biography);
        }
        if (!phone.equals("") && phone.length() < 16) {
            db.collection("users").document(user.getUid())
                    .update("phone", phone);
        }
        if (!password.equals("")) {
            user.updatePassword(password);
        }

        nicknameEditText.setText("");
        biographyEditText.setText("");
        phoneEditText.setText("");
        passwordEditText.setText("");
        repeatPasswordEditText.setText("");
        isEditing = !isEditing;

        callPage();
    }

    private void callPage() {
        if (!isEditing){
            insertUserData();
            recyclerView.setVisibility(View.VISIBLE);
            nicknameTextView.setVisibility(View.VISIBLE);
            biographyTextView.setVisibility(View.VISIBLE);
            nicknameEditText.setVisibility(View.INVISIBLE);
            biographyEditText.setVisibility(View.INVISIBLE);
            phoneEditText.setVisibility(View.INVISIBLE);
            passwordEditText.setVisibility(View.INVISIBLE);
            repeatPasswordEditText.setVisibility(View.INVISIBLE);
            saveButton.setVisibility(View.INVISIBLE);
        } else {
            recyclerView.setVisibility(View.INVISIBLE);
            nicknameTextView.setVisibility(View.INVISIBLE);
            biographyTextView.setVisibility(View.INVISIBLE);
            nicknameEditText.setVisibility(View.VISIBLE);
            biographyEditText.setVisibility(View.VISIBLE);
            phoneEditText.setVisibility(View.VISIBLE);
            passwordEditText.setVisibility(View.VISIBLE);
            repeatPasswordEditText.setVisibility(View.VISIBLE);
            saveButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        callPage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            image = data.getData();
            try {
                inputStream = getActivity().getApplicationContext().getContentResolver().openInputStream(image);
                if (image != null) {
                    StorageReference fileReference = storageRef.child(System.currentTimeMillis() + "." + getFileExtension(image));
                    fileReference.putFile(image).addOnSuccessListener(taskSnapshot -> Objects.requireNonNull(Objects.requireNonNull(taskSnapshot.getMetadata()).getReference()).getDownloadUrl().addOnSuccessListener(uri -> {
                        db.collection("users").document(user.getUid()).update("avatar", uri.toString());
                    })).addOnProgressListener(snapshot -> Toast.makeText(getActivity().getApplicationContext(), "Uploading image", Toast.LENGTH_SHORT)).addOnFailureListener(e -> Toast.makeText(getActivity().getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertPostData(ArrayList<Post> postArrayList){
        db.collection("users").document(user.getUid())
                .collection("post")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                            postArrayList.add(new Post(document.getString("message"),
                                    user.getUid(),
                                    document.contains("image") ? document.getString("image") : null,
                                    (List<String>) document.getData().get("reactions"),
                                    document.getId(),
                                    document.getString("publishDate"),
                                    document.getString("media_content_type")));
                        }
                        Collections.sort(postArrayList, Post.PostPublishDateComparator);
                        PostListAdapter listAdapter = new PostListAdapter(postArrayList, getContext());
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setAdapter(listAdapter);
                    }
                });
    }

    public void insertUserData() {
        ArrayList<Post> postArrayList = new ArrayList<>();

        db.collection("users").document(user.getUid())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        try {
                            assert document != null;
                            insertPostData(postArrayList);
                            nicknameTextView.setText(document.getString("nickname"));
                            biographyTextView.setText(document.getString("biography"));
                            nicknameEditText.setHint(document.getString("nickname"));
                            biographyEditText.setHint(document.getString("biography"));
                            phoneEditText.setHint(document.getString("phone"));
                            Picasso.get().load(document.getString("avatar")).fit().centerCrop().into(avatarImageView);
                        } catch (Exception e) {
                            System.out.println(e.toString());
                        }
                    }
                });
    }
}