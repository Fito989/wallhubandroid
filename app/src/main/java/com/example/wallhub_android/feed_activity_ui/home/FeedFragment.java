package com.example.wallhub_android.feed_activity_ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wallhub_android.NewPostActivity;
import com.example.wallhub_android.adapters.PostListAdapter;
import com.example.wallhub_android.classes.Post;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class FeedFragment extends Fragment {

    private com.example.wallhub_android.databinding.FragmentHomeBinding binding;

    RecyclerView recyclerView;
    FloatingActionButton newPostFloatingButton;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = com.example.wallhub_android.databinding.FragmentHomeBinding.inflate(inflater, container, false);
        recyclerView = binding.postRecyclerView;
        newPostFloatingButton = binding.newPostFloatingActionButton;

        newPostFloatingButton.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), NewPostActivity.class);
            startActivity(intent);
        });

        initActivity();

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        initActivity();
    }

    public void initActivity() {
        ArrayList<Post> postArrayList = new ArrayList<>();

        if (user != null) {
            db.collection("users").document(user.getUid())
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            assert document != null;
                            try {
                                List<String> friends = (List<String>) Objects.requireNonNull(document.getData()).get("friends");
                                for (int i = 0; i < Objects.requireNonNull(friends).size(); i++) {
                                    insertPostData(friends.get(i), postArrayList);
                                }
                            } catch (Exception e) {
                                System.out.println(e.toString());
                            }
                        }
                    });
        }
    }

    public void insertPostData(String documentId, ArrayList<Post> postArrayList){
        db.collection("users").document(documentId)
                .collection("post")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                            postArrayList.add(new Post(document.getString("message"),
                                    documentId,
                                    document.contains("image") ? document.getString("image") : null,
                                    (List<String>) document.getData().get("reactions"),
                                    document.getId(),
                                    document.getString("publishDate"),
                                    document.getString("media_content_type")));
                        }
                        Collections.sort(postArrayList, Post.PostPublishDateComparator);
                        PostListAdapter listAdapter = new PostListAdapter(postArrayList, getContext());
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setAdapter(listAdapter);
                    }
                });
    }
}

