package com.example.wallhub_android.feed_activity_ui.friends;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wallhub_android.UserAreaActivity;
import com.example.wallhub_android.adapters.FriendsListAdapter;
import com.example.wallhub_android.databinding.FragmentFriendsBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FriendsFragment extends Fragment {

    private FragmentFriendsBinding binding;

    RecyclerView recyclerView;
    EditText searchFriendsEditText;
    ImageView searchFriendsImageView;
    ArrayList<String> stringArrayList = new ArrayList<>();

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentFriendsBinding.inflate(inflater, container, false);

        recyclerView = binding.friendsRecyclerView;
        searchFriendsEditText = binding.searchFriendsEditText;
        searchFriendsImageView = binding.searchFriendsImageView;

        searchFriendsImageView.setOnClickListener(v -> db.collection("users")
                .whereEqualTo("nickname", searchFriendsEditText.getText().toString().trim())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Intent intent = new Intent(getContext(), UserAreaActivity.class);
                            intent.putExtra("id", document.getId());
                            requireContext().startActivity(intent);
                        }
                    }
                }));

        if (user != null) {
            List<? extends UserInfo> profile = user.getProviderData();
            db.collection("users").whereEqualTo("email",profile.get(0).getEmail())
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                try {
                                    List<String> friends = (List<String>) document.getData().get("friends");
                                    for (int i = 0; i < Objects.requireNonNull(friends).size(); i++) {
                                        if (!friends.get(i).equals(user.getUid()))
                                        stringArrayList.add(friends.get(i));

                                        FriendsListAdapter listAdapter = new FriendsListAdapter(stringArrayList, getContext());
                                        recyclerView.setHasFixedSize(true);
                                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                                        recyclerView.setAdapter(listAdapter);
                                    }
                                } catch (Exception e) {
                                    System.out.println(e.toString());
                                }
                            }
                        }
                    });
        }

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}