package com.example.wallhub_android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.regex.Pattern;


public class SignUpActivity extends AppCompatActivity {
    private static final String TAG = "SignUpActivity";
    TextView newUser;
    MaterialButton logIn;
    TextInputEditText emailEditText, passwordEditText, confirmPasswordEditText;
    TextInputEditText firstNameEditText, lastNameEditText, nicknameEditText;
    TextInputEditText phoneEditText, birthDateEditText;
    ImageView backImageView;
    Random rn = new Random();
    List<String> randomAvatar = new ArrayList<>();

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        logIn = findViewById(R.id.inicioSesion);
        newUser = findViewById(R.id.nuevoUsuario);
        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        confirmPasswordEditText = findViewById(R.id.confirmPasswordEditText);
        firstNameEditText = findViewById(R.id.nameTextField);
        lastNameEditText = findViewById(R.id.lastNameTextField);
        nicknameEditText = findViewById(R.id.nicknameTextField);
        phoneEditText = findViewById(R.id.phoneTextField);
        birthDateEditText = findViewById(R.id.birthDateTextField);
        backImageView = findViewById(R.id.backImageView);

        backImageView.setOnClickListener(v -> finish());

        newUser.setOnClickListener(v -> finish());

        logIn.setOnClickListener(v -> validate());

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        fillAvatarList();

        //TODO if Api works implement it
//        String url = "http://wallhub-test-api.epizy.com/public/random-image";
//
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
//                (Request.Method.GET, url, null, response ->
//                        System.out.println(response.toString()),
//                        error -> {
//                });

//        ApiClass.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    public void validate() {
        String email = Objects.requireNonNull(emailEditText.getText()).toString().trim();
        String password = Objects.requireNonNull(passwordEditText.getText()).toString().trim();
        String confirmPassword = Objects.requireNonNull(confirmPasswordEditText.getText()).toString().trim();
        String nickname = Objects.requireNonNull(nicknameEditText.getText()).toString().trim();
        String firstName = Objects.requireNonNull(firstNameEditText.getText()).toString().trim();
        String lastName = Objects.requireNonNull(lastNameEditText.getText()).toString().trim();
        String phone = Objects.requireNonNull(phoneEditText.getText()).toString().trim();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailEditText.setError("Invalid mail");
            return;
        } else {
            emailEditText.setError(null);
        }
        if (nickname.length() < 17 && firstName.length() < 31 && lastName.length() < 31 && phone.length() < 16) {
            if (password.isEmpty() || password.length() < 8) {
                passwordEditText.setError("It need minimum 8 characters");
                return;
            } else if (!Pattern.compile("[0-9]").matcher(password).find()) {
                passwordEditText.setError("Minimum 1 number");
                return;
            } else if (!Pattern.compile("[A-Z]").matcher(password).find()) {
                passwordEditText.setError("Minimum 1 upper case");
                return;
            } else {
                passwordEditText.setError(null);
            }

            if (!confirmPassword.equals(password)) {
                confirmPasswordEditText.setError("May be equals");
            } else {
                registrar(email, password);
            }
        } if (nickname.length() > 16) {
            nicknameEditText.setError("Max length: 16 characters");
        } if (firstName.length() > 30) {
            firstNameEditText.setError("Max length: 30 characters");
        } if (lastName.length() > 30) {
            lastNameEditText.setError("Max length: 30 characters");
        } if (phone.length() > 15) {
            phoneEditText.setError("Max length: 15 characters");
        }
    }

    public void registrar(String email, String password){
        Map<String, Object> userData = new HashMap<>();
        List<String> friends = new ArrayList<>();
        userData.put("nickname", Objects.requireNonNull(nicknameEditText.getText()).toString());
        userData.put("firstName", Objects.requireNonNull(firstNameEditText.getText()).toString());
        userData.put("lastName", Objects.requireNonNull(lastNameEditText.getText()).toString());
        userData.put("email", Objects.requireNonNull(emailEditText.getText()).toString());
        userData.put("phone", Objects.requireNonNull(phoneEditText.getText()).toString());
        userData.put("birthDate", Objects.requireNonNull(birthDateEditText.getText()).toString());
        userData.put("avatar", getRandomAvatar());

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()){
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName(Objects.requireNonNull(nicknameEditText.getText()).toString())
                                .build();

                        assert user != null;
                        user.updateProfile(profileUpdates);
                        user.sendEmailVerification()
                                .addOnCompleteListener(task12 -> {
                                    if (task12.isSuccessful()) {
                                        Toast.makeText(SignUpActivity.this, "Verification Email Sent", Toast.LENGTH_LONG).show();
                                    }
                                });
                        friends.add(user.getUid());
                        userData.put("friends", friends);
                        db.collection("users").document(user.getUid()).set(userData);
                        finish();
                    } else {
                        Toast.makeText(SignUpActivity.this, "Registration Failed", Toast.LENGTH_LONG).show();
                    }
                });

    }

    String getRandomAvatar() {
        int randomNum = rn.nextInt(randomAvatar.size()) + 1;
        return randomAvatar.get(randomNum);
    }

    void fillAvatarList() {
        db.collection("avatars")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                            randomAvatar.add(document.getString("url"));
                        }
                    }
                });
    }
}