package com.example.wallhub_android.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wallhub_android.PostActivity;
import com.example.wallhub_android.R;
import com.example.wallhub_android.UserAreaActivity;
import com.example.wallhub_android.classes.Post;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.ViewHolder>{
    private ArrayList<Post> mData;
    private final Context context;

    public PostListAdapter(ArrayList<Post> mData, Context context) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        this.mData = mData;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public PostListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_element, parent, false);
        return new PostListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostListAdapter.ViewHolder holder, int position) {
        holder.bindData(mData.get(position));
    }



    public void setItems(ArrayList<Post> items) {
        mData = items;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        String iconImage;
        ImageView iconImageView, brickImageView, shareImageView, postMediaImageView, postMediaVideoView;
        TextView name, message, reactions, comments, releaseDate;
        CardView cv;
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        public ViewHolder(View itemView) {
            super(itemView);
            iconImageView = itemView.findViewById(R.id.iconImageView);
            brickImageView = itemView.findViewById(R.id.brickImageView);
            shareImageView = itemView.findViewById(R.id.shareImageView);
            name = itemView.findViewById(R.id.nameTextView);
            message = itemView.findViewById(R.id.messageTextView);
            cv = itemView.findViewById(R.id.cv);
            releaseDate = itemView.findViewById(R.id.releaseDateTextView);
            reactions = itemView.findViewById(R.id.numReactionsTextView);
            comments = itemView.findViewById(R.id.numCommentsTextView);
            postMediaImageView = itemView.findViewById(R.id.postMediaImageView);
            postMediaVideoView = itemView.findViewById(R.id.postMediaVideoView);
        }

        void bindData(final Post item) {
            brickImageView.setColorFilter(Color.GRAY);
            if(item.getReactions().contains(user.getUid())){
                brickImageView.clearColorFilter();
            }
            getPostUserNicknameAndImage(item.getPostOwner());
            message.setText(item.getMessage());
            releaseDate.setText(item.getPublishDate());
            reactions.setText(item.getReactionsNumber()+"");
            getPostCommentsCount(item.getPostOwner(), item.getId());
            if (item.getMedia() != null) {
                if (!item.getMedia().equals("")) {
                    if (item.getIsImage() != null) {
                        if (item.getIsImage().equals("image")) {
                            postMediaImageView.setVisibility(View.VISIBLE);
                            Picasso.get().load(item.getMedia()).fit().centerCrop().into(postMediaImageView);
                        } else {
                            postMediaVideoView.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }


            postMediaVideoView.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getMedia()));
                context.startActivity(browserIntent);
            });

            name.setOnClickListener(v -> {
                Intent intent = new Intent(context, UserAreaActivity.class);
                intent.putExtra("Post", item);
                context.startActivity(intent);
            });

            cv.setOnClickListener(v -> {
                Intent intent = new Intent(context, PostActivity.class);
                intent.putExtra("Post", item);
                context.startActivity(intent);

            });



            brickImageView.setOnClickListener(v -> {
                if (item.getReactions().contains(user.getUid())){
                    brickImageView.setColorFilter(Color.GRAY);
                    removeReaction(item);
                    reactions.setText(item.getReactionsNumber() + "");
                } else {
                    brickImageView.clearColorFilter();
                    addReaction(item);
                    reactions.setText(item.getReactionsNumber() + "");
                }
            });
        }

        void addReaction(Post post) {
            List<String> newReactions = post.getReactions();
            newReactions.add(user.getUid());
            db.collection("users").document(post.getPostOwner())
                    .collection("post")
                    .document(post.getId())
                    .update("reactions", newReactions);

            post.setReactions(newReactions);

        }

        void removeReaction(Post post) {
            List<String> newReactions = post.getReactions();
            newReactions.remove(user.getUid());
            db.collection("users").document(post.getPostOwner())
                    .collection("post")
                    .document(post.getId())
                    .update("reactions", newReactions);

            post.setReactions(newReactions);

        }

        void getPostUserNicknameAndImage(String postOwner) {
            DocumentReference docRef = db.collection("users").document(postOwner);
            docRef.get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    assert document != null;
                    if (document.exists()) {
                        name.setText(document.getString("nickname"));
                        iconImage = document.getString("avatar");
                        Picasso.get().load(iconImage).fit().centerCrop().into(iconImageView);
                    }
                }
            });
        }
        void getPostCommentsCount(String postOwner, String postId) {
            db.collection("users").document(postOwner)
                    .collection("post").document(postId)
                    .collection("comment")
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            int i = 0;
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                i++;
                            }
                            comments.setText(i+"");
                        }
                    });
        }
    }
}
