package com.example.wallhub_android.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wallhub_android.R;
import com.example.wallhub_android.UserAreaActivity;
import com.example.wallhub_android.classes.Comment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.ViewHolder>{
    private ArrayList<Comment> mData;
    private final Context context;

    public CommentListAdapter(ArrayList<Comment> mData, Context context) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        this.mData = mData;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public CommentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list_element, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentListAdapter.ViewHolder holder, int position) {
        holder.bindData(mData.get(position));
    }



    public void setItems(ArrayList<Comment> items) {
        mData = items;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iconImage, brickImageView;
        TextView name, message, reactions, comments, releaseDate;
        CardView cv;
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        public ViewHolder(View itemView) {
            super(itemView);
            iconImage = itemView.findViewById(R.id.iconImageView);
            brickImageView = itemView.findViewById(R.id.brickImageView);
            name = itemView.findViewById(R.id.nameTextView);
            message = itemView.findViewById(R.id.messageTextView);
            cv = itemView.findViewById(R.id.cv);
            reactions = itemView.findViewById(R.id.numReactionsTextView);
            comments = itemView.findViewById(R.id.numCommentsTextView);
            releaseDate = itemView.findViewById(R.id.releaseDateTextView);
        }

        void bindData(final Comment item) {
            brickImageView.setColorFilter(Color.GRAY);
            if(item.getReactions().contains(user.getUid())){
                brickImageView.clearColorFilter();
            }
            message.setText(item.getContent());
            reactions.setText(item.getReactionsNumber()+"");
            releaseDate.setText(item.getReleaseDate());
            getCommentUserNicknameAndImage(item.getUserOwner());

            name.setOnClickListener(v -> {
                Intent intent = new Intent(context, UserAreaActivity.class);
                intent.putExtra("Comment", item);
                context.startActivity(intent);
            });

            brickImageView.setOnClickListener(v -> {
                if (item.getReactions().contains(user.getUid())){
                    brickImageView.setColorFilter(Color.GRAY);
                    removeReaction(item);
                    reactions.setText(item.getReactionsNumber() + "");
                } else {
                    brickImageView.clearColorFilter();
                    addReaction(item);
                    reactions.setText(item.getReactionsNumber() + "");
                }
            });
        }

        void getCommentUserNicknameAndImage(String postOwner) {
            DocumentReference docRef = db.collection("users").document(postOwner);

            docRef.get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (Objects.requireNonNull(document).exists()) {
                        name.setText(document.getString("nickname"));
                        Picasso.get().load(document.getString("avatar")).fit().centerCrop().into(iconImage);
                    }
                }
            });
        }

        void addReaction(Comment comment) {
            List<String> newReactions = comment.getReactions();
            newReactions.add(user.getUid());
            db.collection("users").document(comment.getPostOwner())
                    .collection("post")
                    .document(comment.getPostId())
                    .collection("comment")
                    .document(comment.getId())
                    .update("reactions", newReactions);

            comment.setReactions(newReactions);
        }

        void removeReaction(Comment comment) {
            List<String> newReactions = comment.getReactions();
            newReactions.remove(user.getUid());
            db.collection("users").document(comment.getPostOwner())
                    .collection("post")
                    .document(comment.getPostId())
                    .collection("comment")
                    .document(comment.getId())
                    .update("reactions", newReactions);

            comment.setReactions(newReactions);
        }
    }
}
