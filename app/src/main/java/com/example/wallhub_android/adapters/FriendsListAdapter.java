package com.example.wallhub_android.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wallhub_android.R;
import com.example.wallhub_android.UserAreaActivity;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.ViewHolder>{
    private ArrayList<String> mData;
    Context context;

    public FriendsListAdapter(ArrayList<String> mData, Context context) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        this.mData = mData;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public FriendsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friends_list_element, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendsListAdapter.ViewHolder holder, int position) {
        holder.bindData(mData.get(position));
    }



    public void setItems(ArrayList<String> items) {
        mData = items;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iconImage;
        TextView name;
        CardView cv;
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        public ViewHolder(View itemView) {
            super(itemView);
            iconImage = itemView.findViewById(R.id.iconImageView);
            name = itemView.findViewById(R.id.nameTextView);
            cv = itemView.findViewById(R.id.cv);
        }

        void bindData(final String item) {
            getCommentUserNicknameAndImage(item);
        }


        void getCommentUserNicknameAndImage(String userId) {
            DocumentReference docRef = db.collection("users").document(userId);

            cv.setOnClickListener(v -> {
                Intent intent = new Intent(context, UserAreaActivity.class);
                intent.putExtra("id", userId);
                context.startActivity(intent);
            });

            docRef.get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    assert document != null;
                    if (document.exists()) {
                        name.setText(document.getString("nickname"));
                        Picasso.get().load(document.getString("avatar")).fit().centerCrop().into(iconImage);
                    }
                }
            });
        }
    }
}

