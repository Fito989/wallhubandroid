package com.example.wallhub_android;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class NewPostActivity extends AppCompatActivity {

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    StorageReference storageRef = FirebaseStorage.getInstance().getReference("/");
    EditText messageContent;
    boolean haveImages = false;
    Button postButton, addImageButton;
    Uri image;
    boolean isImage = false;
    InputStream inputStream;
    TextView selectedFileTextView;
    String imageStorageReference;
    ImageView backImageView, miniImageImageView;
    public static final int PICK_IMAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        backImageView = findViewById(R.id.backImageView);
        miniImageImageView = findViewById(R.id.miniImageImageView);
        messageContent = findViewById(R.id.messageEditTextTextMultiLine);
        postButton = findViewById(R.id.postButton);
        addImageButton = findViewById(R.id.addImageButton);
        selectedFileTextView = findViewById(R.id.selectedFileTextView);

        backImageView.setOnClickListener(v -> {
            Intent intent = new Intent(NewPostActivity.this, FeedActivity.class);
            startActivity(intent);
            finish();
        });

        addImageButton.setOnClickListener(v -> {
            if (!haveImages) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("*/*");

                Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("*/*");

                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                startActivityForResult(chooserIntent, PICK_IMAGE);
            }
        });

        postButton.setOnClickListener(v -> {
                if (haveImages) {
                        if (image != null) {
                            StorageReference fileReference = storageRef.child(System.currentTimeMillis() + "." + getFileExtension(image));
                            fileReference.putFile(image).addOnSuccessListener(taskSnapshot -> Objects.requireNonNull(Objects.requireNonNull(taskSnapshot.getMetadata()).getReference()).getDownloadUrl().addOnSuccessListener(uri -> {
                                imageStorageReference = uri.toString();
                                createPost(true);
                            })).addOnProgressListener(snapshot -> Toast.makeText(getApplicationContext(), "Uploading image", Toast.LENGTH_SHORT)).addOnFailureListener(e -> Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show());
                        } else {
                            Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show();
                        }

                }
            else if (!messageContent.getText().toString().isEmpty()) {
                createPost(false);
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == PICK_IMAGE) {
                haveImages = true;
                image = data.getData();

                try {
                    inputStream = getApplicationContext().getContentResolver().openInputStream(image);
                    miniImageImageView.setImageURI(image);
                    selectedFileTextView.setVisibility(View.VISIBLE);
                    if (image.toString().contains("image%") ||
                        image.toString().contains("image") ||
                        image.toString().contains("picture") ||
                        image.toString().contains("picture%")){
                        isImage = true;
                    }
                } catch (FileNotFoundException e) {
                    haveImages = false;
                    e.printStackTrace();
                }
            }
        }
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void createPost(boolean hasMedia) {
        Map<String, Object> post = new HashMap<>();
        List<String> reactions = new ArrayList<>();
        post.put("message", messageContent.getText().toString().trim());
        post.put("post_owner", Objects.requireNonNull(mAuth.getCurrentUser()).getUid());
        post.put("publishDate", new SimpleDateFormat("dd/MM/yy", Locale.getDefault()).format(new Date()));
        post.put("reactions", reactions);
        post.put("media", hasMedia);
        if (imageStorageReference != null) {
            post.put("image", imageStorageReference);
            if (isImage) {
                post.put("media_content_type", "image");
            } else {
                post.put("media_content_type", "video");
            }
        }

        db.collection("users").document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                .collection("post")
                .add(post)
                .addOnSuccessListener(documentReference -> db.collection("users").document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                        .collection("post")
                        .document(documentReference.getId())
                        .update("post_id", documentReference.getId())
                .addOnSuccessListener(unused -> {
                    Intent intent = new Intent(NewPostActivity.this, FeedActivity.class);
                    startActivity(intent);
                    finish();
                }))
                .addOnFailureListener(e -> {
                    Toast toast = Toast.makeText(NewPostActivity.this, "Something went wrong", Toast.LENGTH_SHORT);
                    toast.show();
                });
    }
}