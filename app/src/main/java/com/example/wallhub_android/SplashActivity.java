package com.example.wallhub_android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        updateUI(FirebaseAuth.getInstance().getCurrentUser());

    }
    private void updateUI(FirebaseUser account) {
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            Intent intent;
            if (account != null && account.isEmailVerified()) {
                intent = new Intent(SplashActivity.this, FeedActivity.class);
            } else {
                intent = new Intent(SplashActivity.this, MainActivity.class);
            }
            startActivity(intent);
            finish();
        }, 1000);
    }
}