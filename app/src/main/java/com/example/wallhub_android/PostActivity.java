package com.example.wallhub_android;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wallhub_android.adapters.CommentListAdapter;
import com.example.wallhub_android.classes.Comment;
import com.example.wallhub_android.classes.Post;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;


public class PostActivity extends AppCompatActivity {

    TextView content, nickname, reactions;
    RecyclerView recyclerView;
    Post post;
    EditText commentEditText;
    ImageView commentImageView, postImageImageView, backImageView, avatarImageView, brickImageView, deletePostImageView;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        post = (Post) getIntent().getSerializableExtra("Post");

        postImageImageView = findViewById(R.id.postImageImageView);
        nickname = findViewById(R.id.nicknameTextView);
        content = findViewById(R.id.contentTextView);
        avatarImageView = findViewById(R.id.avatarImageView);
        recyclerView = findViewById(R.id.commentRecyclerView);
        commentImageView = findViewById(R.id.commentImageView);
        commentEditText = findViewById(R.id.commentEditText);
        backImageView = findViewById(R.id.backImageView);
        brickImageView = findViewById(R.id.brickImageView);
        reactions = findViewById(R.id.numReactionsTextView);
        deletePostImageView = findViewById(R.id.deletePostImageView);

        if (post.getPostOwner().equals(user.getUid())){
            deletePostImageView.setVisibility(View.VISIBLE);
        }

        deletePostImageView.setOnClickListener(v -> {
            db.collection("users")
                    .document(user.getUid())
                    .collection("post")
                    .document(post.getId()).delete();
            Intent intent = new Intent(PostActivity.this, FeedActivity.class);
            startActivity(intent);
            finish();
        });

        reactions.setText(post.getReactionsNumber()+"");

        backImageView.setOnClickListener(v -> {
            Intent intent = new Intent(PostActivity.this, FeedActivity.class);
            startActivity(intent);
            finish();
        });
        brickImageView.setColorFilter(Color.GRAY);
        if(post.getReactions().contains(user.getUid())){
            brickImageView.clearColorFilter();
        }
        brickImageView.setOnClickListener(v -> {
            if (post.getReactions().contains(user.getUid())){
                brickImageView.setColorFilter(Color.GRAY);
                removeReaction(post);
                reactions.setText(post.getReactionsNumber() + "");
            } else {
                brickImageView.clearColorFilter();
                addReaction(post);
                reactions.setText(post.getReactionsNumber() + "");
            }
        });

        nickname.setOnClickListener(v -> {
                Intent intent = new Intent(PostActivity.this, UserAreaActivity.class);
                intent.putExtra("id", post.getPostOwner());
                startActivity(intent);

        });

        DocumentReference docRef = db.collection("users").document(post.getPostOwner());

        if (post.getMedia() != null) {
            if (!post.getMedia().equals("")) {
                ViewGroup.LayoutParams params= recyclerView.getLayoutParams();
                params.height= 800;
                recyclerView.setLayoutParams(params);
                postImageImageView.setVisibility(View.VISIBLE);
                if (post.getIsImage().equals("image"))
                Picasso.get().load(post.getMedia()).into(postImageImageView);
                else{
                    postImageImageView.setOnClickListener(v -> {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.getMedia()));
                        startActivity(browserIntent);
                    });
                }
            }
        }

        commentImageView.setOnClickListener(v -> {
            if (commentEditText.getText().toString().length() > 0) {
                Map<String, Object> comment = new HashMap<>();
                List<String> reactions = new ArrayList<>();
                comment.put("comment_owner", Objects.requireNonNull(mAuth.getCurrentUser()).getUid());
                comment.put("message", commentEditText.getText().toString().trim());
                comment.put("reactions", reactions);
                comment.put("release_date",  new SimpleDateFormat("dd/MM/yy", Locale.getDefault()).format(new Date()));

                docRef.collection("post").document(post.getId())
                        .collection("comment")
                        .add(comment)
                        .addOnSuccessListener(documentReference -> {
                            docRef.collection("post")
                                    .document(post.getId())
                                    .collection("comment")
                                    .document(documentReference.getId())
                                    .update("comment_id", documentReference.getId());
                            commentEditText.setText("");
                            Toast.makeText(PostActivity.this, "Post commented!", Toast.LENGTH_SHORT).show();
                            updateComments();
                        })
                        .addOnFailureListener(e -> {
                            commentEditText.setText("");
                            Toast.makeText(PostActivity.this, "Something went wrong, try again", Toast.LENGTH_SHORT).show();
                        });
            } else {
                Toast.makeText(PostActivity.this, "Comment cannot be empty", Toast.LENGTH_SHORT).show();
            }
        });

        docRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (Objects.requireNonNull(document).exists()) {
                    nickname.setText(document.getString("nickname"));
                    Picasso.get().load(document.getString("avatar")).fit().centerCrop().into(avatarImageView);
                }
            }
        });

        content.setText(post.getMessage());

        updateComments();
        }

        public void updateComments() {
            ArrayList<Comment> commentArrayList = new ArrayList<>();

            db.collection("users").document(post.getPostOwner())
                    .collection("post").document(post.getId()).collection("comment")
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                try {
                                    commentArrayList.add(new Comment(document.getString("comment_owner"),
                                            document.getString("message"),
                                            document.getString("release_date"),
                                            (List<String>) document.getData().get("reactions"),
                                            document.getId(),
                                            post.getId(),
                                            post.getPostOwner()));
                                    Collections.sort(commentArrayList, Comment.CommentPublishDateComparator);
                                    CommentListAdapter listAdapter = new CommentListAdapter(commentArrayList, this);
                                    recyclerView.setHasFixedSize(true);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                                    recyclerView.setAdapter(listAdapter);
                                } catch (Exception e) {
                                    System.out.println(e.toString());
                                }
                            }
                        }
                    });
        }

    void addReaction(Post post) {
        List<String> newReactions = post.getReactions();
        newReactions.add(user.getUid());
        db.collection("users").document(post.getPostOwner())
                .collection("post")
                .document(post.getId())
                .update("reactions", newReactions);

        post.setReactions(newReactions);

    }

    void removeReaction(Post post) {
        List<String> newReactions = post.getReactions();
        newReactions.remove(user.getUid());
        db.collection("users").document(post.getPostOwner())
                .collection("post")
                .document(post.getId())
                .update("reactions", newReactions);

        post.setReactions(newReactions);

    }
    }

